import React from 'react';
import { Link as RouterLink, Redirect } from 'react-router-dom';
import * as Yup from 'yup';
import { useFormik } from 'formik';
import {
    Box,
    Button,
    Container,
    TextField,
    Typography,
    makeStyles
} from '@material-ui/core';
import axios from 'axios';
import Page from '../../components/Page';

import Swal from 'sweetalert2'

const level = [
    {
        value: '1',
        label: 'ADMIN',
    },
    {
        value: '2',
        label: 'MODERATOR',
    },
    {
        value: '3',
        label: 'USER',
    },
]

const useStyles = makeStyles( (theme) => ({
    root: {
        backgroundColor: theme.palette.background.dark,
        height: '100%',
        paddingBottom: theme.spacing(3),
        paddingTop: theme.spacing(3)
    }
}));

const validationSchema = Yup.object({
    username: Yup
        .string()
        .max(50)
        .required('Username is required'),
    password: Yup
        .string('Enter password')
        .min(8, "Password should be of minimum 8 characters length")
        .required("Password is required"),
    fullname: Yup
        .string()
        .required('Fullname is required'),
    email: Yup
        .string()
        .email('Enter a valid email')
        .required('Email is required'),
    level: Yup
        .string('Select Level')
        .required('Level is required'),
})

export default function Register() {

    const classes = useStyles();
    const [_values, setValues] = React.useState({
        isLoading: false,
        authError: false,
        redirect: false,
        username:'',
        password:'',
        email:'',
        fullname:'',
        level:''
        
    });

    // untuk membuat alert success login
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    const formik = useFormik({
        initialValues: {
            username: _values.username,
            password: _values.password,
            email: _values.email,
            fullname: _values.fullname,
            level: _values.level,
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            //alert(JSON.stringify(values.username))
            setValues({ ..._values, isLoading: true });
            
            const url = 'http://localhost/resto-pos-api/index.php/auth/registerUser';
            
            let data = new FormData();
            data.set('username', values.username);
            data.set('password', values.password);
            data.set('email', values.email);
            data.set('fullname', values.fullname);
            data.set('level', values.level);

            //alert(data);

            axios.post(url,data)
                .then( (result) => {
                    // console.log(result);
                    // setValues({ ..._values, isLoading: false });
                    if(result.data.status === true) { // jika login berhasil

                        //console.log(result.data);
                        localStorage.setItem('token', result.data.token);
                        localStorage.setItem('isLogin', true);
                        setValues({ ...values, isLoading: false, redirect:true });
                        return Toast.fire({ icon: 'success', title: result.data.message})

    
                    } else { // jika login gagal
    
                        setValues({ ...values, isLoading: false});
                        //console.log(result.data);
                        return Toast.fire({ icon: 'warning', title: result.data.message})
                    }
                })
                .catch( (error) => {
                    console.log(error);
                })
        }
    })


    const renderRedirect = () => {
        if(_values.redirect) {
            return <Redirect to='/' />
            //return Toast.fire({ icon: 'success', title: 'Signed in successfully'})
        }
    }


    const isLoading = _values.isLoading;

    return (
        <Page
            classes={classes.root}
            title="Register"
        >

            <Box
                display="flex"
                flexDirection="column"
                height="100%"
                justifyContent="center"
            >
                <Container maxWidth="sm">

                            <form onSubmit={formik.handleSubmit}>
                                <Box mb={3}>
                                    <Typography
                                        color="textPrimary"
                                        variant="h2"
                                    >
                                        Create New Account
                                    </Typography>
                                </Box>

                                <TextField 
                                    error={Boolean(formik.touched.fullname && formik.errors.fullname)}
                                    fullWidth
                                    helperText={formik.touched.fullname && formik.errors.fullname}
                                    label="Fullname"
                                    margin="normal"
                                    name="fullname"
                                    onBlur={formik.handleBlur}
                                    onChange={formik.handleChange}
                                    value={formik.values.fullname}
                                    variant="outlined"
                                />

                                <TextField 
                                    error={Boolean(formik.touched.username && formik.errors.username)}
                                    fullWidth
                                    helperText={formik.touched.username && formik.errors.username}
                                    label="Username"
                                    margin="normal"
                                    name="username"
                                    onBlur={formik.handleBlur}
                                    onChange={formik.handleChange}
                                    value={formik.values.username}
                                    variant="outlined"
                                />

                                <TextField 
                                    error={Boolean(formik.touched.password && formik.errors.password)}
                                    fullWidth
                                    helperText={formik.touched.password && formik.errors.password}
                                    label="Password"
                                    margin="normal"
                                    name="password"
                                    onBlur={formik.handleBlur}
                                    onChange={formik.handleChange}
                                    value={formik.values.password}
                                    variant="outlined"
                                />
                                <TextField 
                                    error={Boolean(formik.touched.email && formik.errors.email)}
                                    fullWidth
                                    helperText={formik.touched.email && formik.errors.email}
                                    label="Email"
                                    margin="normal"
                                    name="email"
                                    onBlur={formik.handleBlur}
                                    onChange={formik.handleChange}
                                    value={formik.values.email}
                                    variant="outlined"
                                    type="email"
                                />
                                <TextField 
                                    error={Boolean(formik.touched.level && formik.errors.level)}
                                    fullWidth
                                    select
                                    helperText={formik.touched.level && formik.errors.level}
                                    label="Role"
                                    margin="normal"
                                    name="level"
                                    onBlur={formik.handleBlur}
                                    onChange={formik.handleChange}
                                    value={formik.values.level}
                                    variant="outlined"
                                    SelectProps= {{
                                        native: true,
                                    }}
                        
                                > 
                                    {level.map( (option) => (
                                        <option key={option.value} value={option.value}>{option.label}</option>
                                    ))}
                                </TextField>

                                <Box my={2}>
                                    <Button
                                        color="primary"
                                        fullWidth
                                        size="large"
                                        type="submit"
                                        variant="contained"
                                    >
                                        Sign up now &nbsp;&nbsp;&nbsp;
                                        {
                                            isLoading ? ( <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>) : (<span></span>)
                                        }
                                    </Button>
                                </Box>
                                

                            </form>
                            {renderRedirect()}

                </Container>
                
            </Box>

        </Page>
    )




}


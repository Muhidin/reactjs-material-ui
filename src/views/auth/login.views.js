import React from 'react';
import { Link as RouterLink, Redirect } from 'react-router-dom';
import * as Yup from 'yup';
import { useFormik } from 'formik';
import {
    Box,
    Button,
    Container,
    TextField,
    Typography,
    makeStyles,
    InputAdornment,
    IconButton
} from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import axios from 'axios';
import Page from '../../components/Page';

import Swal from 'sweetalert2'

const useStyles = makeStyles( (theme) => ({
    root: {
        backgroundColor: theme.palette.background.dark,
        height: '100%',
        paddingBottom: theme.spacing(3),
        paddingTop: theme.spacing(3)
    }
}));

const validationSchema = Yup.object({
    username: Yup
        .string()
        .required('Username is required'),
    password: Yup
        .string()
        .required('Password is required')
})

export default function Login() {
    const classes = useStyles();
    const [_values, setValues] = React.useState({
        isLoading: false,
        redirect: false,
        username: '',
        password: '',
        showPassword: false
    })

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    const formik = useFormik({
        initialValues: {
            username: _values.username,
            password: _values.password,
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            setValues({ ..._values, isLoading: true });

            const url = 'http://localhost/resto-pos-api/index.php/auth/login';
            
            let data = new FormData();
            data.set('username', values.username);
            data.set('password', values.password);

            axios.post(url, data)
                .then( (result) => {
                    if(result.data.status === true) { // jika login berhasil

                        //console.log(result.data);
                        localStorage.setItem('token', result.data.token);
                        localStorage.setItem('isLogin', true);
                        setValues({ ...values, isLoading: false, redirect:true });
                        return Toast.fire({ icon: 'success', title: result.data.message})

    
                    } else { // jika login gagal
    
                        setValues({ ...values, isLoading: false});
                        //console.log(result.data);
                        return Toast.fire({ icon: 'warning', title: result.data.message})
                    }
                })
                .catch( (error) => {
                    return Toast.fire({ icon: 'warning', title: error})
                })
        }
    })

    const renderRedirect = () => {
        if(_values.redirect) {
            return <Redirect to='/dashboard' />
            //return Toast.fire({ icon: 'success', title: 'Signed in successfully'})
        }
    }

    const handleClickShowPassword = () => {
        setValues({ ..._values, showPassword: !_values.showPassword });
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const isLoading = _values.isLoading;

    return (
        <Page   
            classes={classes.root}
            title="Login"
        >
            <Box
                display="flex"
                flexDirection="column"
                height="100%"
                justifyContent="center"
            >
                <Container maxWidth="sm">
                    <form onSubmit={formik.handleSubmit}>
                        <Box mb={3}>
                            <Typography
                                color="textPrimary"
                                variant="h5" >
                                Login System
                            </Typography>
                        </Box>

                        <TextField 
                            error={Boolean(formik.touched.username && formik.errors.username)}
                            fullWidth
                            helperText={formik.touched.username && formik.errors.username}
                            label="Username"
                            name="username"
                            margin="normal"
                            onBlur={formik.handleBlur}
                            onChange={formik.handleChange}
                            value={formik.values.username}
                            variant="outlined"
                        />

                                <TextField 
                                    error={Boolean(formik.touched.password && formik.errors.password)}
                                    fullWidth
                                    helperText={formik.touched.password && formik.errors.password}
                                    label="Password"
                                    margin="normal"
                                    name="password"
                                    onBlur={formik.handleBlur}
                                    onChange={formik.handleChange}
                                    value={formik.values.password}
                                    variant="outlined"
                                    type={_values.showPassword ? 'text' : 'password'}
                                    InputProps= {{
                                        endAdornment: <InputAdornment position="end">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={handleClickShowPassword}
                                                onMouseDown={handleMouseDownPassword}
                                            >
                                                {_values.showPassword ? <Visibility /> : <VisibilityOff />}
                                            </IconButton>
                                        </InputAdornment> ,
                                    }}
                                />

                            <Button  variant="contained" color="primary" type="submit" disabled={_values.isLoading ? true : false }>Login &nbsp;&nbsp;&nbsp;
                                {
                                isLoading ? ( <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>) : (<span></span>)
                                }
                            </Button>
                    </form>
                    {renderRedirect()}
                </Container>

            </Box>
        </Page>
    )
}
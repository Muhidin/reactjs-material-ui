// import logo from './logo.svg';
import './App.css';
import Login from './components/Login';
import Dashboard from './components/dashboard.component';
import DataMenu from './components/kasir/data_menu.component';
import ResponsiveDrawer from './components/responsiveDrawer';
import Register from './views/auth/register.views';

import { BrowserRouter as Router, Route,Switch } from 'react-router-dom';



function App() {
  return (
    <div className="App">
        <Router>
          <Switch>
            <Route exact path='/' component={Login} />
            <Route  path='/index' component={Login} />
            <Route  path='/dashboard' component={Dashboard} />
            <Route path='/menu' component={DataMenu} />
            <Route path='/drawer' component={ResponsiveDrawer} />
            <Route path='/register' component={Register} />
          </Switch>
        </Router>
    </div>
  );
}

export default App;

import React,{Component} from 'react';
import axios from 'axios';
import ProductMenu from '../kasir/menu.component';


import Grid from '@material-ui/core/Grid';
import { Redirect } from 'react-router-dom';

export default class DataMenu extends Component{

    

    state = {
        menu:[],
    }

    componentDidMount(){
        const url = 'http://localhost/resto-pos-api/index.php/api/menu'
        axios.get(url)
            .then(res => {
                const menu = res.data.data;
                this.setState({ menu: menu });
            })
            .catch(error => {
                console.log(error);
            })
    }

    render() {
        let token = localStorage.getItem('token');

        if(!token) {
            return(
                <Redirect to='/' />
            )

        } else {
            return(
                <Grid container spacing={1}>
                    
                    {this.state.menu.map( (listMenu) => 
                        <Grid item xs={3} >
                                <ProductMenu
                                    id={listMenu.id_menu}
                                    image={listMenu.image}
                                    title={listMenu.menu}
                                    price={listMenu.price}
                                    menu={listMenu.menu}
                                />
                        </Grid>
                    
                    )}

                 </Grid>

            )
        }
            
        
    }
        
    
}
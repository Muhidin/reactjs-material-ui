import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import { CardContent, Typography } from '@material-ui/core';





const useStyles = makeStyles({
    root: {
        maxWidth: 200,
        marginTop:20,
    },
    media: {
        height: 120,
        paddingLeft:5,
    },
});



export default function ProductMenu(props) {
    const classes = useStyles();


        return(

            <Card className={classes.root} onClick={() => console.log(props.id) } >
                <CardActionArea>
                    <CardMedia
                        className={classes.media}
                        // image={this.props.image}
                        // title={this.props.menu}
                        image={props.image}
                        title={props.title}
                    >
                    </CardMedia>
                </CardActionArea>
                <CardContent>
                <Typography variant="body2" color="textSecondary" component="p">
                    <strong>{props.menu} :</strong><span>{props.price}</span>
                </Typography>
            </CardContent>
            </Card>
        
        );

}
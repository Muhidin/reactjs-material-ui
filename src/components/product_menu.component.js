import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import { CardContent, Typography } from '@material-ui/core';

const useStyles = makeStyles({
    root: {
        maxWidth: 200,
        marginTop:20,
    },
    media: {
        height: 120,
    },
});

export default function ProductMenu() {
    const classes = useStyles();

    return(
    <div className="container-fluid">
    <Card className={classes.root} onClick={() => console.log('test') } >
        <CardActionArea>
            <CardMedia
                className={classes.media}
                // image={this.props.image}
                // title={this.props.menu}
                image="https://images.immediate.co.uk/production/volatile/sites/30/2020/08/flat-white-3402c4f.jpg?webp=true&quality=90&resize=500%2C454"
                title="coffee late"
            >
            </CardMedia>
        </CardActionArea>
        <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
            <strong>Coffee Late :</strong><span>12,000</span>
        </Typography>
      </CardContent>
    </Card>
    </div>
    );
}
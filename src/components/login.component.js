import React,{Component} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Redirect } from 'react-router-dom';

import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';

import { red } from '@material-ui/core/colors';

import axios from 'axios';

export default class Login extends Component {

    state = {
        username: '',
        password:'',
        isLoading: false,
        redirect:false,
        authError: false,
        showPassword: false,
    }


    hdlUsername = event => {
        this.setState({ username: event.target.value });
    };

    hdlPwd = event => {
        this.setState({ password: event.target.value })
    }

    hdlSbmt = event => {
        event.preventDefault();
        this.setState({ isLoading: true });

        const url = 'http://localhost/resto-pos-api/index.php/auth/login';
        const usr = this.state.username;
        const pwd = this.state.password;

        let frmData = new FormData();
        frmData.set('username', usr);
        frmData.set('password', pwd);

        axios.post(url, frmData)
            .then(result => {
                //console.log(result.data);
                if(result.data.status === true) { // jika login berhasil

                    //console.log(result.data);
                    localStorage.setItem('token', result.data.token);
                    localStorage.setItem('isLogin', true);
                    this.setState({ isLoading : false, redirect: true});


                } else { // jika login gagal

                    this.setState({ isLoading : false})
                    console.log(result.data);
                }
            })
            .catch(error => {
                alert('Login failed');
                this.setState({isLoading:false, authError: true})
            })
    }


    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/dashboard' />
        }
    };


    render(){
        const isLoading = this.state.isLoading;
        return (
            <div className="container">

                <br/><br/><br/>

                <div className="card card-login mx-auto md-6">
                    <div className="card-header">Login</div>
                    <div className="card-body">
                        <form onSubmit={this.hdlSbmt}>
                            <div className="form-group">
                                <div className="form-label-group">

                                    <TextField
                                        className="form-control" 
                                        id="username"
                                        label="Username"
                                        type="text"
                                        name="username"
                                        onChange={this.hdlUsername}
                                        
                                    />

                                </div>
                            </div>
                            <div className="form-group">
                                <div className="form-label-group">
                                    <TextField type="password" 
                                    className={"form-control " + (this.state.authError ? 'is-invalid' : '')} 
                                    id="inputPassword" label="password" 
                                    name="password" onChange={this.hdlPwd} required/>
                                    <div className="invalid-feedback">
                                        Please provide a valid Password.
                                    </div>

                                    <TextField
                                        className="form-control" 
                                        label="Password"
                                        id="password"
                                        type={values.showPassword ? 'text' : 'password'}
                                        onChange={this.hdlPwd}
                                        InputProps= {{
                                            endAdornment: <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowPassword}
                                                    onMouseDown={handleMouseDownPassword}
                                                >
                                                    {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                                                </IconButton>
                                            </InputAdornment> ,
                                        }}
                                        
                                    />
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="checkbox">
                                    <label>
                                        <input type="checkbox" value="remember-me"/>Remember Password
                                    </label>
                                </div>
                            </div>
                            <div className="form-group">
                                {/* <button className="btn btn-primary btn-block" type="submit" disabled={this.state.isLoading ? true : false}>Login &nbsp;&nbsp;&nbsp;
                                    {isLoading ? (
                                        <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                    ) : (
                                        <span></span>
                                    )}
                                </button> */}
                                <Button variant="contained" color="primary" type="submit" disabled={this.state.isLoading ? true : false }>Login &nbsp;&nbsp;&nbsp;
                                        {
                                            isLoading ? ( <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>) : (<span></span>)
                                        }
                                </Button>
                            </div>
                            
                        </form>

                    </div>
                </div>
                {this.renderRedirect()}

            </div>

        );
    }
}
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';

import axios from 'axios';

import Swal from 'sweetalert2'

import {Redirect} from 'react-router-dom';

// untuk membuat alert success login
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
})

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        marginTop: theme.spacing(10),
    },
    margin: {
        margin: theme.spacing(1),
    },
    withoutLabel:{
        marginTop: theme.spacing(3),
    },
    btLogin: {
        backgroundColor: 'orange',
    },

}));


export default function Login() {
    const classes = useStyles();
    const [values, setValues] = React.useState({
        isLoading: false,
        authError: false,
        username: '',
        password: '',
        showPassword: false,
        redirect: false,
        loginFailed: false,
    });


    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
      };

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        const username = values.username;
        const password = values.password;
        
        setValues({ ...values, isLoading: true }); // di set true supaya loading muncul

        
        const url = 'http://localhost/resto-pos-api/index.php/auth/login';
        let data = new FormData()
        data.set('username', username);
        data.set('password', password);
        axios.post(url, data)
            .then(result => {
                //console.log(values.isLoading);

                if(result.data.status === true) { // jika login berhasil

                    //console.log(result.data);
                    localStorage.setItem('token', result.data.token);
                    localStorage.setItem('isLogin', true);
                    setValues({ ...values, isLoading: false, redirect:true });


                } else { // jika login gagal

                    setValues({ ...values, isLoading: false, loginFailed: true});
                    //console.log(result.data);
                    return Toast.fire({ icon: 'warning', title: result.data.message})
                }
            })
            .catch(error => {
                console.log(error);
                setValues({ ...values, isLoading: false });
            })


    }


    const renderRedirect = () => {
        if(values.redirect) {
            return <Redirect to='/dashboard' />
            //return Toast.fire({ icon: 'success', title: 'Signed in successfully'})
        }
    }

    const isLoading = values.isLoading

        return (
            <div className={classes.root}>
                <br/><br/><br/>
                <div className="card card-login mx-auto md-6">
                    <div className="card-header">Login-System</div>
                        <div className="card-body">
    
                        <form onSubmit={handleSubmit}>
                            <div className="form-group">
                                <TextField
                                    required
                                    className="form-control" 
                                    id="username"
                                    label="Username"
                                    type="text"
                                    name="username"
                                    onChange={handleChange('username')}
                                    value= {values.username}
                                />
                            </div>
    
                            <div className="form-group">
                                <TextField
                                    required
                                    className="form-control" 
                                    label="Password"
                                    id="password"
                                    type={values.showPassword ? 'text' : 'password'}
                                    value={values.password}
                                    onChange={handleChange('password')}
                                    InputProps= {{
                                        endAdornment: <InputAdornment position="end">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={handleClickShowPassword}
                                                onMouseDown={handleMouseDownPassword}
                                            >
                                                {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                            </IconButton>
                                        </InputAdornment> ,
                                    }}
                                    
                                />
    
                            </div>
    
                            <Button className={classes.btLogin} variant="contained" color="primary" type="submit" disabled={values.isLoading ? true : false }>Login &nbsp;&nbsp;&nbsp;
                                {
                                isLoading ? ( <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>) : (<span></span>)
                                }
                            </Button>
                            
                        </form>
                        
    
                    </div>

                    {renderRedirect()}
                    
                </div>
            </div>
        )

}
    



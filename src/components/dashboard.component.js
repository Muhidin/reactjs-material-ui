import React, { Component } from 'react';
import {Redirect } from 'react-router-dom';
import { decodeJWT } from '../midleware/decodeToken';
import Header from './header.component';

// material
import PropTypes from 'prop-types';
import { 
    AppBar,
    CssBaseline,
    Divider,
    Drawer,
    Hidden,
    IconButton,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    MenuIcon,
    Toolbar,
    Typography

} from '@material-ui/core';

import { makeStyles, useTheme } from '@material-ui/core/styles';


const drawerWidth = 200;

const useStyles = makeStyles( (theme) => ({
    root: {
        display: 'flex',
    },
    drawer: {
        [
            theme.breakpoints.up('sm')
        ]: {
            width: drawerWidth,
            flexShrink: 0
        },
    },
    appBar: {
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        }
    },
    menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
          display: 'none',
        },
      },
      // necessary for content to be below app bar
      toolbar: theme.mixins.toolbar,
      drawerPaper: {
        width: drawerWidth,
      },
      content: {
        flexGrow: 1,
        padding: theme.spacing(3),
      },

}))


// function Dashboard(props) {
    
//     const { window } = props;
//     const classes = useStyles();
//     const theme = useTheme();
//     const [mobileOpen, setMobileOpen ] = React.useState(false);

//     const handleDrawerOpen = () => {
//         setMobileOpen(!mobileOpen);
//     }
// }

export default class Dashboard extends Component {

    render() {
        let token = localStorage.getItem('token')

        if(!token){
            return (
                <Redirect to='/' />
            )

        } else {
            const data_user = decodeJWT(token);

            localStorage.setItem('username', data_user.username);
            localStorage.setItem('id_user', data_user.id_user);
            localStorage.setItem('expired_in', data_user.expired_in);
            //localStorage.setItem('level', data_user.level);

            

            return (
                <div>
                    <Header />
                </div>
            )
        }

    }
}